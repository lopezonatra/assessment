<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
namespace App\View\Helper;
use CakeDC\Users\Controller\Component\UsersAuthComponent;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\Helper;
/**
 * User helper
 */
class UserHelper extends Helper
{
    public $helpers = ['Html', 'Form', 'Users.AuthLink'];
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    /**
     * Social login link
     *
     * @param string $name
     * @param array $options
     * @return string
     */
    public function socialLogin($name, $options = [])
    {

        if (empty($options['label'])) {
            $options['label'] = __d('Users', 'Sign in with');
        }


        $icon = $this->Html->tag('i', '', [
            'class' => __d('Users', 'fa fa-{0}', strtolower($name)),
        ]);
        if (isset($options['title'])) {
            $providerTitle = $options['title'];
        } else {
            $providerTitle = __d('Users', '{0} {1}', Hash::get($options, 'label'), Inflector::camelize($name));
        }
        $providerClass = __d(
            'Users',
            'btn btn-social btn-{0} ' . Hash::get($options, 'class') ?: '',
            strtolower($name)
        );

        return $this->Html->link($icon . $providerTitle, "users/".$name."Login", [
            'escape' => false, 'class' => $providerClass
        ]);
    }
    /**
     * Social Links
     *
     * @param array $providerOptions Provider
     * @return array Links
     */
    public function socialLoginList(array $providerOptions = [])
    {
        $outProviders = [];
        $providers = Configure::read('OAuth.providers');
        foreach ($providers as $provider => $options) {

            if (!empty($options['options']['redirectUri']) &&
                !empty($options['options']['clientId']) &&
                !empty($options['options']['clientSecret'])
            ) {

                if (isset($providerOptions[$provider])) {

                    $options['options'] = Hash::merge($options['options'], $providerOptions[$provider]);
                }
                $outProviders[] = $this->socialLogin($provider, $options['options']);
            }
        }
        return $outProviders;
    }

}