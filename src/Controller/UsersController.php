<?php
namespace App\Controller;

use App\Controller\AppController;
use \League\OAuth2\Client\Provider\Facebook;
use Cake\Core\Configure;

/**
 * Users Controller
 *
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login','register','facebookLogin','socialLogin']);
    }
    public function isAuthorized($user)
    {
        if(isset($user['role']) and $user['role'] === 'customer')
        {
            if(in_array($this->request->action, ['home','logout']))
            {
                return true;
            }

            if(in_array($this->request->action, ['view', 'edit']))
            {
                $id = (int) $this->request->params['pass'][0];
                if ($id == $user['id']) {
                    return true;
                }
            }
        }

        if(isset($user['role']) and $user['role'] === 'agent' )
        {
            if(in_array($this->request->action, ['home', 'view', 'logout', 'index' ]))
            {
                return true;
            }

            if(in_array($this->request->action, ['edit']))
            {
                $id = (int) $this->request->params['pass'][0];
                if ($id == $user['id']) {
                    return true;
                }
            }
        }

        return parent::isAuthorized($user);
    }

    public function login()
    {
        if($this->request->is('post'))
        {
            $user = $this->Auth->identify();
            if($user)
            {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            else
            {
                $this->Flash->error('Invalid username or password, try again', ['key' => 'auth']);
            }
        }
        if ($this->Auth->user())
        {
            return $this->redirect(['controller' => 'Users', 'action' => 'home']);
        }
    }


    public function facebookLogin()
    {

        if ($this->Auth->user())
        {
            exit;
            return $this->redirect(['controller' => 'Users', 'action' => 'home']);
        }



        $providerName = 'facebook';
        $provider = new Facebook( Configure::read("OAuth.providers.$providerName.options"));

        if($this->request->getQuery('code')){


            $token = $provider->getAccessToken('authorization_code', [
                'code' => $this->request->getQuery('code')
            ]);

            $data = $provider->getResourceOwner($token);

            $existsUser = $this->Users->find()
                ->where(['email' => (empty($data->getEmail())) ? $data->getId().'@facebook.com' : $data->getEmail() ])
                ->first();

            if (!empty($existsUser)) {
                $this->Auth->setUser($existsUser->toArray()) ;
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $user = $this->Users->newEntity();
                $usersTable = $this->Users;
                $usersTable->registerSocial($user, $providerName, $data);

                return $this->redirect(['controller' => 'Users', 'action' => 'home']);
            }
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }

        $authUrl = $provider->getAuthorizationUrl([
            'scope' => ['email'],
        ]);

        $this->redirect($authUrl);
    }




    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    public function home()
    {
        $this->render();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {
            $usersTable = $this->Users;
            $requestData = $this->request->getData();
            if ($userSaved = $usersTable->register($user, $requestData)){
                $this->Flash->success(__('A validation e-mail has been sent to your e-mail address .Please validate your account before log in'));
                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The user could not be registered. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {
           $user = $this->Users->patchEntity($user, $this->request->getData());
           if ($this->Users->save($user)) {
               $this->Flash->success(__('The user has been saved.'));

               return $this->redirect(['action' => 'index']);
           }
           $this->Flash->error(__('The user could not be saved. Please, try again.'));
       }
       $this->set(compact('user'));
       $this->set('_serialize', ['user']);

    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }



    /**
     * Validate an email
     *
     * @param string $token token
     * @return void
     */
    public function validateEmail($token = null)
    {
        try {
            $result = $this->Users->validate($token, 'activateUser');
            if ($result) {
                $this->Flash->success(__d('Users', 'User account validated successfully'));
            } else {
                $this->Flash->error(__d('Users', 'User account could not be validated'));
            }
        } catch (UserAlreadyActiveException $exception) {
            $this->Flash->error(__d('Users', 'User already active'));
        }
        return $this->redirect(['action' => 'login']);
    }


}
