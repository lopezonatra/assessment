<?php

namespace App\Mailer;

use Cake\Datasource\EntityInterface;
use Cake\Mailer\Email;
use Cake\Mailer\Mailer;

/**
 * User Mailer
 *
 */
class UsersMailer extends Mailer
{
    /**
     * Send the templated email to the user
     *
     * @param EntityInterface $user User entity
     * @return void
     */
    protected function validation(EntityInterface $user)
    {
        $firstName = isset($user['first_name'])? $user['first_name'] . ', ' : '';
        $user->setHidden(['password', 'token_expires', 'api_token']);
        $subject = 'Your account validation link';
        $this
            ->to($user['email'])
            ->setSubject($firstName . $subject)
            ->setViewVars($user->toArray())
            ->setTemplate('validation');
    }
}