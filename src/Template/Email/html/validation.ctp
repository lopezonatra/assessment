<?php

$activationUrl = [
    '_full' => true,
    'plugin' => 'Users',
    'controller' => 'Users',
    'action' => 'validateEmail',
    isset($token) ? $token : ''
];
?>
<p>
    <?= __d('App/Users', "Hi {0}", isset($first_name) ? $first_name : '') ?>,
</p>
<p>
    <strong><?= $this->Html->link(__d('Users', 'Activate your account here'), $activationUrl) ?></strong>
</p>
<p>
    <?= __d(
        'App/Users',
        "If the link is not correctly displayed, please copy the following address in your web browser {0}",
        $this->Url->build($activationUrl)
    ) ?>
</p>
<p>
    <?= __d('Users', 'Thank you') ?>,
</p>