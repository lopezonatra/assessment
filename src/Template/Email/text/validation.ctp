<?php

$activationUrl = [
    '_full' => true,
    'controller' => 'Users',
    'action' => 'validateEmail',
    isset($token) ? $token : ''
];
?>
<?= __d('Users', "Hi {0}", isset($first_name) ? $first_name : '') ?>,

<?= __d(
    'Users',
    "Please copy the following address in your web browser {0}",
    $this->Url->build($activationUrl)
) ?>

<?= __d('Users', 'Thank you') ?>,
