<div class="users form">
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
<fieldset>
    <legend><?= __('Please enter your email and password') ?></legend>
    <?= $this->Form->input('email') ?>
    <?= $this->Form->input('password') ?>
</fieldset>
<?= $this->Form->button(__('Login')); ?>
<?=    $this->Html->link("Register", array('controller' => 'Users','action'=> 'register'), array( 'class' => 'button')) ?>
<?= $this->Form->end() ?>
<?= implode(' ', $this->User->socialLoginList()); ?>
</div>