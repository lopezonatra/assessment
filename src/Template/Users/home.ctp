

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>

        <?php if($current_user['role'] === 'admin' or $current_user['role'] === 'agent'  ): ?>
            <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <?php endif; ?>
        <?php if($current_user['role'] === 'admin' ): ?>
            <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <?php endif; ?>

        <li><?= $this->Html->link(__('Edit personal info'), ['controller' => 'Users', 'action' => 'edit', $current_user['id']]) ?> </li>

    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <h2>Bienvenido <?= $this->Html->link($current_user['first_name'] . ' ' . $current_user['last_name'], ['controller' => 'Users', 'action' => 'view', $current_user['id']]) ?></h2>
</div>