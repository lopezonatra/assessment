<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>

        <?php if($current_user['role'] === 'admin' or $current_user['role'] === 'agent'  ): ?>
            <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <?php endif; ?>
        <?php if($current_user['role'] === 'admin' ): ?>
            <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <?php endif; ?>
        <li><?= $this->Html->link(__('Edit personal info'), ['controller' => 'Users', 'action' => 'edit', $current_user['id']]) ?> </li>

        <?php if($current_user['role'] === 'admin' ): ?>
            <li class="heading"><?= __('Current User Actions') ?></li>
            <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
            <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
            <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
            <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <?php endif; ?>


    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->first_name . ' ' . $user->last_name ) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($user->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($user->last_name) ?></td>
        </tr>
        <?php if($current_user['role'] === 'admin' or $current_user['role'] === 'agent' ): ?>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <?php endif; ?>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= h($user->role) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $user->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
