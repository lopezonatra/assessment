<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php if($current_user['role'] === 'admin' or $current_user['role'] === 'agent'  ): ?>
            <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <?php endif; ?>
        <?php if($current_user['role'] === 'admin' ): ?>
            <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <?php endif; ?>
        <li><?= $this->Html->link(__('Edit personal info'), ['controller' => 'Users', 'action' => 'edit', $current_user['id']]) ?> </li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('role' , ['options' => ['admin' => 'Administrator','agent' => 'Agent','customer' => 'Customer']]);
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
