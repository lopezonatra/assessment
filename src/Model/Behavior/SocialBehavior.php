<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Datasource\EntityInterface;


class SocialBehavior extends Behavior
{


    /**
     * Registers an  social user.
     *
     * @param EntityInterface $user
     * @param string $provider Provider name.
     * @param array $data User info
     * @return bool|EntityInterface
     */
    public function registerSocial($user, $provider , $data)
    {



        $user->first_name = $data->getFirstName();
        $user->last_name = $data->getLastName();
        $user->email = (empty($data->getEmail())) ? $data->getId().'@facebook.com' : $data->getEmail() ;
        $user->role = 'customer';
        $user->password = 'default01';
        $user->active = true;
        $user->provider = $provider;
        $user->activation_date = new \DateTime();
        $result = $this->_table->save($user);
        return $result;

    }

}