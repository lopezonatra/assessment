<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Time;


class TokenBehavior extends Behavior
{

    /**
     * Update active and token based on validateEmail param
     *
     * @param EntityInterface $user
     * @param bool $validateEmail email
     * @param int $tokenExpiration
     * @return EntityInterface
     */
    protected function _updateActive(EntityInterface $user, $validateEmail, $tokenExpiration)
    {
        $emailValidated = $user['validated'];
        if (!$emailValidated && $validateEmail) {
            $user['active'] = false;
            $user->updateToken($tokenExpiration);
        } else {
            $user['active'] = true;
            $user['activation_date'] = new Time();
        }
        return $user;
    }
    /**
     * Remove user token
     *
     * @param EntityInterface $user
     * @return EntityInterface
     */
    protected function _removeValidationToken(EntityInterface $user)
    {
        $user->token = null;
        $user->token_expires = null;
        $result = $this->_table->save($user);
        return $result;
    }

}