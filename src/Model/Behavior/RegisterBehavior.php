<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Datasource\EntityInterface;
use Cake\Mailer\MailerAwareTrait;

class RegisterBehavior extends TokenBehavior
{
    use MailerAwareTrait;

    /**
     * Registers an user.
     *
     * @param EntityInterface $user
     * @param array $data User info
     * @return bool|EntityInterface
     */
    public function register($user, $data)
    {
        $validateEmail = true;
        $tokenExpiration = 3600;
        $user = $this->_table->patchEntity(
            $user,
            $data
        );
        $user['role'] = 'customer';
        $user->validated = false;
        $user = $this->_updateActive($user, $validateEmail, $tokenExpiration);
        $user = $this->_updateActive($user, $validateEmail, $tokenExpiration);
        $this->_table->isValidateEmail = $validateEmail;
        $userSaved = $this->_table->save($user);
        if ($userSaved && $validateEmail) {
            $this->_sendValidationEmail($user);
        }
        return $userSaved;
    }

    /**
     * Validates token and return user
     *
     * @param string $token
     * @param null $callback
     * @return EntityInterface $user
     */
    public function validate($token, $callback = null)
    {
        $user = $this->_table->find()
            ->select(['token_expires', 'id', 'active', 'token'])
            ->where(['token' => $token])
            ->first();
        if (empty($user)) {
            //@todo UserNotFoundException
        }
        if ($user->tokenExpired()) {
            //@todo TokenExpiredException
        }
        if (!method_exists($this, $callback)) {
            return $user;
        }
        return $this->_table->{$callback}($user);
    }

    /**
     * Activates an user
     *
     * @param EntityInterface $user
     * @return User entity or  false if the user could not be activated
     */
    public function activateUser(EntityInterface $user)
    {
        if ($user->active) {
            //UserAlreadyActiveException;
        }
        $user->activation_date = new \DateTime();
        $user->token_expires = null;
        $user->active = true;
        $result = $this->_table->save($user);
        return $result;
    }

    /**
     * Wrapper for mailer
     *
     * @param EntityInterface $user
     * @return void
     */
    protected function _sendValidationEmail($user)
    {
        $mailer = 'Users';
        $this
            ->getMailer($mailer)
            ->send('validation', [$user]);
    }


}