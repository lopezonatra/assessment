<?php
use Migrations\AbstractMigration;

class UpdateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('token', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
        ->addColumn('token_expires', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('activation_date', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->changeColumn('role', 'enum', [
            'values' => 'admin, agent, customer',
            'default' => 'customer'
            ])
        ->update();
    }
}
