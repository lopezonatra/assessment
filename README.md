# Assesment for  Click Delivery

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

Backend Software Developer Test for Click Delivery using [CakePHP](https://cakephp.org) 3.5.3
Functional basic user management system with sign-up, sign-in and user
management with Facebook log in.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### AWS instance
https://ec2-18-221-147-91.us-east-2.compute.amazonaws.com

### Database DLL/DML
[assessment.sql](database_ddl_dml/assessment.sql)

### Prerequisites

* PHP 5.6 or greater

### Installation

# Installation & Setup

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Download this repository - `git clone git@bitbucket.org/lopezonatra/assessment.git`
3. Install dependencies with composer - `composer install`.
4. Run database script in - `assessment/database_ddl_dml/` .
5. Configure your database credentials in ``app.php``. Make sure to use the same database name as in step 4.
5. Configure your smtp credentials in ``app.php``. 
6. Start the server `bin/cake server -p 8765`.
7. Go to http://localhost:8765 in your browser.


### Notes

* This project uses DefaultPasswordHasher class which uses bcrypt by default.
* For Facebook log in ssl configuration is necessary
* For mail validation smtp configuration is necessary