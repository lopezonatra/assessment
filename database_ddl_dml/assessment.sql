-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-10-2017 a las 00:36:24
-- Versión del servidor: 5.7.19-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `assessment`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20171005021749, 'CreateUsersTable', '2017-10-05 03:16:01', '2017-10-05 03:16:01', 0),
(20171007034204, 'UpdateUsersTable', '2017-10-07 03:53:43', '2017-10-07 03:53:44', 0),
(20171007211513, 'AddColumnsToUsers', '2017-10-07 21:17:22', '2017-10-07 21:17:22', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','agent','customer') NOT NULL DEFAULT 'customer',
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_expires` datetime DEFAULT NULL,
  `activation_date` datetime DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `role`, `active`, `created`, `modified`, `token`, `token_expires`, `activation_date`, `provider`) VALUES
(1, 'Juan Felipe ', 'Lopez Onatra', 'onatra0915@gmail.com', '$2y$10$p8i00XS56AmAFr299wGOTekffZ/ubvtTBFb12DVQgzzaBNWH0o5La', 'admin', 1, '2017-10-05 04:27:20', '2017-10-06 03:42:36', NULL, NULL, NULL, NULL),
(31, 'admin', 'admin', 'admin@assessment.com', '$2y$10$0xCO72tLwXMJFZpE4I0GleyvmtqFKFugtkELaObKi6GtwjQ3GpZ8W', 'admin', 1, '2017-10-08 00:24:00', '2017-10-08 00:24:00', NULL, NULL, NULL, NULL),
(32, 'agent', 'agent', 'agent@assessment.com', '$2y$10$c2ZHh4bdDzonk6BmEK07.u/MQSXqqxy9eZnPYyqR51zzf16pqt1Bi', 'agent', 1, '2017-10-08 00:26:41', '2017-10-08 00:26:41', NULL, NULL, NULL, NULL),
(33, 'customer', 'customer', 'customer@assessment.com', '$2y$10$hSawdPRZBXMIVkN4IcnAc.cSbONZ5tUxjGy7Zluxjjk02TIC76YX6', 'customer', 1, '2017-10-08 00:27:11', '2017-10-08 00:27:11', NULL, NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
